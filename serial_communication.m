%% 清除工作空间及所有串口
clear;
clc;  
delete(instrfindall);   
%% 定义全局变量
global text_time;        %测试时间集合(x axis) 
global Vout_std;         %输出电压平均值及标准差(y axis)        
global Screen_Center;    %屏幕中间的横坐标位置  
global text_number;      %当前的测试计数值
%% 定义串口变量
SerialPort = 'com3';
BaudRate = 115200;
DataBits = 8;
StopBits = 1;
Parity = 'none';
FlowControl = 'none';
DataCount = 1024;
%% 初始化全局变量
text_time = 0;  
Vout_std = [0;0];    
Screen_Center = 0;  
text_number = 1; 
%% 获取绘制线的句柄
plothandle = plot(text_time, Vout_std, '*', 'MarkerSize', 5, 'EraseMode', 'background'); 
legend('mean', 'std');
set(plothandle(1), 'color', 'r');
set(plothandle(2), 'color', 'b');
%% 获取绘制文本的句柄
texthandle1 = text(0, 0, '', 'color', 'r', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'fontsize', 12);
texthandle2 = text(0, 0, '', 'color', 'b', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'fontsize', 12);
%% 图像绘制的参数
axis([Screen_Center - 50 Screen_Center + 50 -10 3500]);  
xlabel('test time(s)', 'FontSize', 14);
ylabel('Vout(mV)', 'FontSize', 14);
title('Serial Assistant', 'FontSize', 14);
grid on;  
%% 尝试建立串口通讯，并定义串口通讯参数         
try
    serialhandle = serial(SerialPort);  
catch  
    error('can not connect to serial');  
end 

set(serialhandle, 'BaudRate', BaudRate, 'DataBits', DataBits, 'StopBits', StopBits, 'Parity', Parity, 'FlowControl', FlowControl);  
serialhandle.InputBufferSize = DataCount;
serialhandle.BytesAvailableFcnCount = DataCount;              
serialhandle.BytesAvailableFcnMode = 'byte';            %缓冲区接收到DataCount个数据后触发回调函数
serialhandle.BytesAvailableFcn = {@callback, plothandle, texthandle1, texthandle2, DataCount};  
%% 开启串口通讯并暂停主程序
fopen(serialhandle);        
pause; 
%% 关闭并删除串口
fclose(serialhandle);  
delete(serialhandle);  
clear serialhandle  
%close all;  
%clear all;  

