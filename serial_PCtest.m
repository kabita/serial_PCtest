function varargout = serial_PCtest(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @serial_PCtest_OpeningFcn, ...
                   'gui_OutputFcn',  @serial_PCtest_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end
% End initialization code - DO NOT EDIT

%% --- Executes just before serial_PCtest is made visible.
function serial_PCtest_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
%% 寻找可用串口
clc;
delete(instrfindall); 
info = instrhwinfo('serial');
str = lower(char(info.SerialPorts));    %返回可用串口号
switch str  
    case 'com1' 
        portnumber = 1;  
    case 'com2'  
        portnumber = 2;  
    case 'com3'  
        portnumber = 3;  
    case 'com4'  
        portnumber = 4;  
    case 'com5'  
        portnumber = 5;  
    case 'com6' 
        portnumber = 6;  
    case 'com7'  
        portnumber = 7;    
    case 'com8' 
        portnumber = 8;  
    case 'com9' 
        portnumber = 9; 
    otherwise
        portnumber = 0;
end
%% 定义全局变量
global text_time;        %测试时间集合(x axis) 
global Vout_std;         %输出电压平均值及标准差(y axis)        
global Screen_Center;    %屏幕中间的横坐标位置  
global text_number;      %当前的测试计数值
global plothandle1;      %图像1操作句柄
global plothandle2;      %图像2操作句柄
global texthandle1;      %图像1上text操作句柄
global texthandle2;      %图像2上text操作句柄
global SerialPort;       %串口号
global BaudRate;         %波特率   
global DataBits;         %数据位
global StopBits;         %停止位
global Parity;           %校验位
global FlowControl;      %流控
global DataCount;        %数据量
global FilePath;         %存储路径
global saveaxes1number;  %保存axes1的次数
global saveaxes2number;  %保存axes2的次数
%% 初始化绘图变量
text_time = 0;  
Vout_std = [0;0];    
Screen_Center = 0;  
text_number = 1; 
%% 初始化串口变量
if portnumber == 0
    SerialPort = '';        %如果没有串口可用，初始化为空
else
    SerialPort = str;   
end
BaudRate = 115200;
DataBits = 8;
StopBits = 1;
Parity = 'none';
FlowControl = 'none';
DataCount = 1024;
saveaxes1number = 1;
saveaxes2number = 1;
%% 初始化存储路径
FilePath = pwd;
%% 初始化弹出式菜单值
if portnumber == 0
    set(handles.COM, 'value', 10); 
    set(handles.pushopen, 'Enable', 'off');         %如果没有串口可用，不能打开串口
    set(handles.pushclose, 'Enable', 'off');
    set(handles.COM, 'Enable', 'off');  
else
    set(handles.COM, 'value', portnumber);
end
set(handles.BaudRates, 'value', 5);  
set(handles.DataBits, 'value', 4); 
set(handles.StopBits, 'value', 1); 
set(handles.Parity, 'value', 1); 
set(handles.FlowControl, 'value', 1); 
set(handles.DataCount, 'value', 1); 
set(handles.pushclose, 'Enable', 'off');
%% 初始化存储路径显示值
set(handles.filepathtext, 'string', FilePath);
%% 定义图像绘制的参数及绘制线的句柄
axes(handles.axes1);
plothandle1 = plot(handles.axes1, text_time, Vout_std(1,:), '*', 'MarkerSize', 5, 'EraseMode', 'background'); 
% axis([Screen_Center - 50 Screen_Center + 50 -10 3500]);  
% ylabel('Vmean(mV)', 'FontSize', 12);
axis([Screen_Center - 50 Screen_Center + 50 -10 844800]);  
ylabel('Vsum(mV)', 'FontSize', 12);
grid on; 
set(plothandle1, 'color', 'r');
texthandle1 = text(0, 0, '', 'color', 'r', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'fontsize', 12);

axes(handles.axes2);
plothandle2 = plot(handles.axes2, text_time, Vout_std(2,:), '*', 'MarkerSize', 5, 'EraseMode', 'background'); 
axis([Screen_Center - 50 Screen_Center + 50 0 1000]);  
xlabel('test time(s)', 'FontSize', 12);
ylabel('Vstd(mV)', 'FontSize', 12);
grid on; 
set(plothandle2, 'color', 'b');
texthandle2 = text(0, 0, '', 'color', 'b', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'fontsize', 12);
end

%% --- Outputs from this function are returned to the command line.
function varargout = serial_PCtest_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;
end

%% --- Executes on button press in pushopen.
function pushopen_Callback(hObject, eventdata, handles)
%% 清除工作空间及所有串口 
clc;
delete(instrfindall);  
%% 定义全局变量
global serialhandle;
global SerialPort;
global BaudRate;
global DataBits;
global StopBits;
global Parity;
global FlowControl;
global DataCount;
%% 尝试建立串口通讯，并定义串口通讯参数
try
    serialhandle = serial(SerialPort);  
catch  
    error('can not connect to serial');  
end 

set(serialhandle, 'BaudRate', BaudRate, 'DataBits', DataBits, 'StopBits', StopBits, 'Parity', Parity, 'FlowControl', FlowControl);  
serialhandle.InputBufferSize = DataCount;
serialhandle.BytesAvailableFcnCount = DataCount;              
serialhandle.BytesAvailableFcnMode = 'byte';            %缓冲区接收到DataCount个数据后触发回调函数
serialhandle.BytesAvailableFcn = {@serialcallback, handles};  
%% 使能开关按键
set(handles.pushopen, 'Enable', 'off');  
set(handles.filepath, 'Enable', 'off');  
set(handles.findserialports, 'Enable', 'off');  
set(handles.pushopen, 'Enable', 'off');  
set(handles.COM, 'Enable', 'off');  
set(handles.BaudRates, 'Enable', 'off');  
set(handles.DataBits, 'Enable', 'off');  
set(handles.StopBits, 'Enable', 'off');  
set(handles.Parity, 'Enable', 'off');  
set(handles.FlowControl, 'Enable', 'off');  
set(handles.DataCount, 'Enable', 'off');  
set(handles.pushclose, 'Enable', 'on');  
%% 打开串口
fopen(serialhandle); 
end
    
%% --- Executes on button press in pushclose.
function pushclose_Callback(hObject, eventdata, handles)
%% 定义全局变量
global serialhandle;
%% 关闭并删除串口
fclose(serialhandle);  
delete(serialhandle);  
%% 使能开关按键
set(handles.pushopen, 'Enable', 'on');  
set(handles.filepath, 'Enable', 'on');  
set(handles.findserialports, 'Enable', 'on');  
set(handles.COM, 'Enable', 'on');  
set(handles.BaudRates, 'Enable', 'on');  
set(handles.DataBits, 'Enable', 'on');  
set(handles.StopBits, 'Enable', 'on');  
set(handles.Parity, 'Enable', 'on');  
set(handles.FlowControl, 'Enable', 'on');  
set(handles.DataCount, 'Enable', 'on'); 
set(handles.pushclose, 'Enable', 'off');  
end

%% -- Executes on selection change in COM.
function COM_Callback(hObject, eventdata, handles)
global SerialPort;   
val=get(hObject, 'value');  
switch val  
    case 1  
        SerialPort = 'com1';  
    case 2  
        SerialPort = 'com2';  
    case 3  
        SerialPort = 'com3';  
    case 4  
        SerialPort = 'com4';  
    case 5  
        SerialPort = 'com5';  
    case 6  
        SerialPort = 'com6';  
    case 7  
        SerialPort = 'com7';    
    case 8  
        SerialPort = 'com8';  
    case 9  
        SerialPort = 'com9';  
end
end

%% --- Executes during object creation, after setting all properties.
function COM_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

%% --- Executes on selection change in BaudRates.
function BaudRates_Callback(hObject, eventdata, handles)
global BaudRate;   
val=get(hObject, 'value');  
switch val  
    case 1  
        BaudRate = 9600;  
    case 2  
        BaudRate = 19200;  
    case 3  
        BaudRate = 39400;  
    case 4  
        BaudRate = 57600;  
    case 5  
        BaudRate = 115200;     
end
end

%% --- Executes during object creation, after setting all properties.
function BaudRates_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

%% --- Executes on selection change in DataBits.
function DataBits_Callback(hObject, eventdata, handles)
global DataBits;   
val=get(hObject, 'value');  
switch val  
    case 1  
        DataBits = 5;  
    case 2  
        DataBits = 6;  
    case 3  
        DataBits = 7;  
    case 4  
        DataBits = 8;    
end
end

%% --- Executes during object creation, after setting all properties.
function DataBits_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

%% --- Executes on selection change in Parity.
function Parity_Callback(hObject, eventdata, handles)
global Parity;   
val=get(hObject, 'value');  
switch val  
    case 1  
        Parity = 'none';  
    case 2  
        Parity = 'even';  
    case 3  
        Parity = 'odd';  
    case 4  
        Parity = 'mark'; 
    case 5
        Parity = 'space';
end
end

%% --- Executes during object creation, after setting all properties.
function Parity_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

%% --- Executes on selection change in StopBits.
function StopBits_Callback(hObject, eventdata, handles)
global StopBits;   
val=get(hObject, 'value');  
switch val  
    case 1  
        StopBits = 1;  
    case 2  
        StopBits = 1.5;  
    case 3  
        StopBits = 2;  
end
end

%% --- Executes during object creation, after setting all properties.
function StopBits_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

%% --- Executes on selection change in FlowControl.
function FlowControl_Callback(hObject, eventdata, handles)
global FlowControl;   
val=get(hObject, 'value');  
switch val  
    case 1  
        FlowControl = 'none';  
    case 2  
        FlowControl = 'rts/cts';  
    case 3  
        FlowControl = 'xon/xoff';  
end
end

%% --- Executes during object creation, after setting all properties.
function FlowControl_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

%% --- Executes on selection change in DataCount.
function DataCount_Callback(hObject, eventdata, handles)
global DataCount;   
val=get(hObject, 'value');  
switch val  
    case 1  
        DataCount = 1024;  
    case 2  
        DataCount = 2048;  
    case 3  
        DataCount = 4096;  
end
end

%% --- Executes during object creation, after setting all properties.
function DataCount_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

%% --- Callback function for serial
function serialcallback(Serialhandle, ~, handles)  
%% 定义全局变量
global text_time;        
global Vout_std;               
global Screen_Center;     
global text_number;      
global plothandle1;
global plothandle2;
global texthandle1;
global texthandle2;
global DataCount;
global FilePath;
%% 定义数据保存路径
rawdata_Path = strcat(FilePath, '\', 'rawdata.csv');                   %原始数据
statisticsdata_Path = strcat(FilePath, '\', 'statisticsdata.csv');     %处理后数据(均值及标准差) 
%% 读取数据并处理，求均值及标准差
rawdata = fread(Serialhandle, DataCount);
j = 1;
data = zeros(1, DataCount./4);
for i = 1 : (length(rawdata) - 3)
	s1 = rawdata(i);
	s2 = rawdata(i+1);
	s3 = rawdata(i+2);
	s4 = rawdata(i+3);
	if s3 == 32 && s4 == 44
        data(j) = s1.*16.*16 + s2;
        if data(j) > 3500
            data(j) = 0;
        end
        j = j + 1;
	end
end

% data_mean = mean(data);     %求均值
data_sum = sum(data);     %求和
data_delta = std(data);     %求标准差    
%% 保存数据至存储路径
dlmwrite(rawdata_Path, data, '-append');
% dlmwrite(statisticsdata_Path, [data_mean, data_delta], '-append');
dlmwrite(statisticsdata_Path, [data_sum, data_delta], '-append');
%% 实时绘图，更新图像数据
text_time = [text_time text_number];  
% Vout_std = [Vout_std [data_mean;data_delta]];  
Vout_std = [Vout_std [data_sum;data_delta]]; 
set(plothandle1, 'XData', text_time, 'YData', Vout_std(1,:));
set(plothandle2, 'XData', text_time, 'YData', Vout_std(2,:)); 
% set(texthandle1, 'Position', [text_number + 1, data_mean + 10], 'String', strcat('mean = ', num2str(data_mean)));
set(texthandle1, 'Position', [text_number + 1, data_sum + 10], 'String', strcat('sum = ', num2str(data_sum)));
set(texthandle2, 'Position', [text_number + 1, data_delta + 10], 'String', strcat('std = ', num2str(data_delta)));
drawnow
%% 修改图像绘制的参数
Screen_Center = Screen_Center + 1;  
% axis(handles.axes1,[Screen_Center - 50 Screen_Center + 50 -10 3500]);  
axis(handles.axes1,[Screen_Center - 50 Screen_Center + 50 -10 844800]);  
axis(handles.axes2,[Screen_Center - 50 Screen_Center + 50 0 1000]);  
text_number = text_number + 1;  
end

%% --- Executes on button press in filepath.
function filepath_Callback(hObject, eventdata, handles)
%% 定义全局变量
global FilePath;
%% 获取目标路径并显示
temppath = uigetdir(FilePath,'存储路径');
if temppath ~= 0
    FilePath = temppath;
end
set(handles.filepathtext, 'string', FilePath);
end

%% --- Executes on button press in findserialports.
function findserialports_Callback(hObject, eventdata, handles)
%% 定义全局变量
global SerialPort;
%% 寻找可用串口
delete(instrfindall); 
info = instrhwinfo('serial');
str = lower(char(info.SerialPorts));
switch str  
    case 'com1' 
        portnumber = 1;  
    case 'com2'  
        portnumber = 2;  
    case 'com3'  
        portnumber = 3;  
    case 'com4'  
        portnumber = 4;  
    case 'com5'  
        portnumber = 5;  
    case 'com6' 
        portnumber = 6;  
    case 'com7'  
        portnumber = 7;    
    case 'com8' 
        portnumber = 8;  
    case 'com9' 
        portnumber = 9; 
    otherwise
        portnumber = 0;
end
%% 修改GUI设置
if portnumber == 0
    set(handles.COM, 'value', 10); 
    set(handles.pushopen, 'Enable', 'off');  
    set(handles.pushclose, 'Enable', 'off');
    set(handles.COM, 'Enable', 'off');  
    SerialPort = '';
else
    set(handles.COM, 'value', portnumber);
    set(handles.COM, 'Enable', 'on'); 
    set(handles.pushopen, 'Enable', 'on');  
    SerialPort = str;
end
end

%% --- Executes on button press in saveaxes1.
function saveaxes1_Callback(hObject, eventdata, handles)
%% 定义全局变量
global FilePath;
global Screen_Center;
global saveaxes1number;
%% 指定路径下存储为png图像
rawdata_png_Path = strcat(FilePath, '\', 'rawdata',num2str(saveaxes1number),'.png');
h=get(handles.axes1,'children');
figure('visible','off');
axes;
% axis([Screen_Center - 50 Screen_Center + 50 -10 3500]);  
axis([Screen_Center - 50 Screen_Center + 50 -10 844800]);  
grid on; 
copyobj(h,gca);
print(gcf,'-zbuffer','-dpng','-r500',rawdata_png_Path);
saveaxes1number = saveaxes1number + 1;
end

%% --- Executes on button press in saveaxes2.
function saveaxes2_Callback(hObject, eventdata, handles)
%% 定义全局变量
global FilePath;
global Screen_Center;
global saveaxes2number;
%% 指定路径下存储为png图像
statisticsdata_png_Path = strcat(FilePath, '\', 'statisticsdata',num2str(saveaxes2number),'.png');
h=get(handles.axes2,'children');
figure('visible','off');
axes;
axis([Screen_Center - 50 Screen_Center + 50 0 1000]);  
grid on; 
copyobj(h,gca);
print(gcf,'-zbuffer','-dpng','-r500',statisticsdata_png_Path);
saveaxes2number = saveaxes2number + 1;
end

%% --- Executes on button press in saveall.
function saveall_Callback(hObject, eventdata, handles)
%% 定义全局变量
global FilePath;
global Screen_Center;
global saveaxes1number;
global saveaxes2number;
%% 指定路径下存储为png图像
rawdata_png_Path = strcat(FilePath, '\', 'rawdata',num2str(saveaxes1number),'.png');
h=get(handles.axes1,'children');
figure('visible','off');
axes;
% axis([Screen_Center - 50 Screen_Center + 50 -10 3500]);  
axis([Screen_Center - 50 Screen_Center + 50 -10 844800]);  
grid on; 
copyobj(h,gca);
print(gcf,'-zbuffer','-dpng','-r500',rawdata_png_Path);
saveaxes1number = saveaxes1number + 1;

statisticsdata_png_Path = strcat(FilePath, '\', 'statisticsdata',num2str(saveaxes2number),'.png');
h=get(handles.axes2,'children');
figure('visible','off');
axes;
axis([Screen_Center - 50 Screen_Center + 50 0 1000]);  
grid on; 
copyobj(h,gca);
print(gcf,'-zbuffer','-dpng','-r500',statisticsdata_png_Path);
saveaxes2number = saveaxes2number + 1;
end
