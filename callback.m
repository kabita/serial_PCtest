function callback(Serialhandle, ~, plothandle, texthandle1, texthandle2, DataCount)  
%% 定义全局变量
    global text_time;  
    global Vout_std;  
    global Screen_Center;  
    global text_number;    
%% 定义数据保存路径
    rawdata_Path = 'rawdata.csv';                   %原始数据
    statisticsdata_Path = 'statisticsdata.csv';     %处理后数据(均值及标准差)  
%% 读取数据并处理，求均值及标准差
    rawdata = fread(Serialhandle, DataCount);
    j = 1;
    data = zeros(1,DataCount./4);
    for i = 1 : (length(rawdata) - 3)
        s1 = rawdata(i);
        s2 = rawdata(i+1);
        s3 = rawdata(i+2);
        s4 = rawdata(i+3);
        if s3 == 32 && s4 == 44
            data(j) = s1.*16.*16 + s2;
        j = j + 1;
        end
    end
    
    data_mean = mean(data);
    data_delta = std(data);    
%% 保存数据至存储路径
    dlmwrite(rawdata_Path, data, '-append');
    dlmwrite(statisticsdata_Path, [data_mean, data_delta], '-append');
%% 实时绘图，更新图像数据
    text_time = [text_time text_number];  
    Vout_std = [Vout_std [data_mean;data_delta]];  
    set(plothandle(1), 'XData', text_time, 'YData', Vout_std(1,:));  
    set(plothandle(2), 'XData', text_time, 'YData', Vout_std(2,:)); 
    set(texthandle1, 'Position', [text_number + 1, data_mean + 10], 'String', strcat('mean = ', num2str(data_mean)));
    set(texthandle2, 'Position', [text_number + 1, data_delta + 10], 'String', strcat('std = ', num2str(data_delta)));
    drawnow         
%% 修改图像绘制的参数
    Screen_Center = Screen_Center + 1;  
    axis([Screen_Center - 50 Screen_Center + 50 -10 3500]);  
    text_number = text_number+1;  
end 